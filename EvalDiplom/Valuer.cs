﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvalDiplom
{
    class Valuer
    {
        public enum Value : int
        {
            Low = 3,
            Medium = 5,
            High = 7
        }

        public Value value;
        public List<AssessmentType> types;

        public Valuer(Value value, List<AssessmentType> types)
        {
            this.value = value;
            this.types = types;
        }

        public double Evaluation
        {
            get
            {
                int sum = 0;
                foreach (AssessmentType type in types)
                    sum += (int)type.value;

                double typeResult = 0;
                foreach (AssessmentType type in types)
                    typeResult += ((double)type.value / sum) * type.Evaluation;

                return typeResult;
            }
        }
    }
}
