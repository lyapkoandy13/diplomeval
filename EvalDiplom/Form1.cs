﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvalDiplom
{
    public partial class Form1 : Form
    {
        public const int MAX_MARK = 7;
        public const int MIN_MARK = 1;
        
        Valuer[] valuers;

        public Form1()
        {
            InitializeComponent();

            AssessmentType honestyType = AssessmentType.fromDictionary(AssessmentType.Value.Low, honesty, comboBox1);
            AssessmentType litWorkType = AssessmentType.fromDictionary(AssessmentType.Value.Low, litWork, comboBox2);
            AssessmentType goalType = AssessmentType.fromDictionary(AssessmentType.Value.Medium, goal, comboBox3);
            AssessmentType samostType = AssessmentType.fromDictionary(AssessmentType.Value.High, samost, comboBox4);
            Valuer chief = new Valuer(Valuer.Value.High, new List<AssessmentType>(new AssessmentType[] { honestyType, litWorkType, goalType, samostType }));


            AssessmentType actualType = AssessmentType.fromDictionary(AssessmentType.Value.High, actual, comboBox5);
            AssessmentType litReviewType = AssessmentType.fromDictionary(AssessmentType.Value.VeryHigh, litReview, comboBox6);
            AssessmentType newnessType = AssessmentType.fromDictionary(AssessmentType.Value.Medium, newness, comboBox7);
            AssessmentType desitionType = AssessmentType.fromDictionary(AssessmentType.Value.VeryHigh, desition, comboBox8);
            AssessmentType gramType = AssessmentType.fromDictionary(AssessmentType.Value.High, gram, comboBox9);
            AssessmentType practType = AssessmentType.fromDictionary(AssessmentType.Value.High, pract, comboBox10);
            AssessmentType dotrStandType = AssessmentType.fromDictionary(AssessmentType.Value.High, dotrStand, comboBox11);
            AssessmentType graphMatType = AssessmentType.fromDictionary(AssessmentType.Value.VeryLow, graphMat, comboBox12);
            AssessmentType naukTehType = AssessmentType.fromDictionary(AssessmentType.Value.Medium, naukTeh, comboBox13);
            Valuer reviewer = new Valuer(Valuer.Value.High, new List<AssessmentType>(new AssessmentType[] { actualType, newnessType, litReviewType, desitionType, practType, dotrStandType, graphMatType, gramType, naukTehType }));


            AssessmentType yakistType = AssessmentType.fromDictionary(AssessmentType.Value.High, yakistDok, comboBox14);
            AssessmentType yakist2Type = AssessmentType.fromDictionary(AssessmentType.Value.High, yakist2, comboBox15);
            AssessmentType yakistVidpowType = AssessmentType.fromDictionary(AssessmentType.Value.High, yakistVidpov, comboBox16);
            AssessmentType zagOcinkaType = AssessmentType.fromDictionary(AssessmentType.Value.VeryHigh, zagOcinka, comboBox17);
            Valuer commission = new Valuer(Valuer.Value.High, new List<AssessmentType>(new AssessmentType[] { yakistType, yakist2Type, yakistVidpowType, zagOcinkaType }));


            valuers = new Valuer[] { chief, reviewer, commission };
        }

        

        public double CalculateResult()
        {
            int sum = 0;
            foreach (Valuer valuer in valuers)
                sum += (int)valuer.value;

            double globalCoeff = 0;
            foreach (Valuer valuer in valuers)
                globalCoeff += (((double)valuer.value / sum) * valuer.Evaluation);

            return MIN_MARK + globalCoeff * (MAX_MARK - MIN_MARK);
        }


        Dictionary<string, int> honesty = new Dictionary<string, int>() {
            { "Несумлінний студент", (int)Variant.Value.NotBad},
            { "Сумлінний студент", (int)Variant.Value.High},
        };

        Dictionary<string, int> litWork = new Dictionary<string, int>() {
            { "Не вміє працювати з літературою", (int)Variant.Value.Bad},
            { "Складності в опрацюванні літератури", (int)Variant.Value.Medium},
            { "Вміє опрацьовувати літературу", (int)Variant.Value.High },
        };

        Dictionary<string, int> goal = new Dictionary<string, int>() {
            { "Не слідував поставленим керівником цілям", (int)Variant.Value.Bad},
            { "Не повністю досяг цілей, поставлених керівником", (int)Variant.Value.NotBad},
            { "Повністю досяг цілей, поставлених керівником", (int)Variant.Value.VeryHigh },
        };

        Dictionary<string, int> samost = new Dictionary<string, int>() {
            { "Низький об'єм самостійних результатів", (int)Variant.Value.Medium},
            { "Невеликий об'єм самостійних результатів", (int)Variant.Value.NotBad},
            { "Великий об'єм самостійних результатів", (int)Variant.Value.VeryHigh},
        };

        // Data for Group2 comboboxes

        Dictionary<string, int> actual = new Dictionary<string, int>() {
            { "Неактуальна робота", (int)Variant.Value.Medium},
            { "Дещо актуальна робота", (int)Variant.Value.Normal},
            { "Актуальна робота", (int)Variant.Value.VeryHigh},
        };

        Dictionary<string, int> newness = new Dictionary<string, int>() {
            { "Застарілі ідеї", (int)Variant.Value.Low},
            { "Перспективна робота", (int)Variant.Value.High},
            { "Оригінальні ідеї", (int)Variant.Value.VeryHigh},
        };

        Dictionary<string, int> litReview = new Dictionary<string, int>() {
            { "Недостатній аналіз", (int)Variant.Value.VeryLow },
            { "Застаріла література", (int)Variant.Value.Medium },
            { "Відчизняна література", (int)Variant.Value.NotBad },
            { "Новітня література", (int)Variant.Value.High },
            { "Новітня відчизняна та зарубіжна література", (int)Variant.Value.VeryHigh},
        };

        Dictionary<string, int> desition = new Dictionary<string, int>() {
            { "Обгрунтування відсутні", (int)Variant.Value.VeryBad},
            { "Недостатні обгрунтування", (int)Variant.Value.Medium},
            { "Обгрунтований тільки однин варіант", (int)Variant.Value.Low },
            { "Представлено декілька варіантів", (int)Variant.Value.NotBad},
            { "Обгрунтований вибір оптимального варіанту", (int)Variant.Value.VeryHigh },
        };

        Dictionary<string, int> pract = new Dictionary<string, int>() {
            { "Немає практичних рекомендацій", (int)Variant.Value.Low },
            { "Результати мають рекомендаційний характер", (int)Variant.Value.High },
            { "Можливе практичне використання", (int)Variant.Value.VeryHigh },
        };

        Dictionary<string, int> graphMat = new Dictionary<string, int>() {
            { "Не розкривають сенс проекту", (int)Variant.Value.Medium },
            { "Не повністю розкривають сенс, присутні похибки в оформленні", (int)Variant.Value.Normal },
            { "Повністю розкривають сенс і відповідать ДСТУ", (int)Variant.Value.VeryHigh },
        };

        Dictionary<string, int> gram = new Dictionary<string, int>() {
            { "Багато стилістичних та граматичних помилок", (int)Variant.Value.VeryLow },
            { "Присутні окремі граматичні помилки", (int)Variant.Value.High },
            { "Текст читається легко, помилки відсутні", (int)Variant.Value.VeryHigh },
        };

        Dictionary<string, int> naukTeh = new Dictionary<string, int>() {
            { "Використання ЕОМ відсутнє", (int)Variant.Value.Low},
            { "Використання ЕОМ носить другорядний характер", (int)Variant.Value.Medium},
            { "Застарілі пакети програм в основній частині", (int)Variant.Value.NotBad },
            { "Сучасні пакети програм в основній частині", (int)Variant.Value.VeryHigh},
            { "Оригінальні програмно-технічні засоби в основній частині роботи", (int)Variant.Value.Excellent},
        };

        Dictionary<string, int> yakistDok = new Dictionary<string, int>() {
            { "Не розкриває якість проекту", (int)Variant.Value.Low },
            { "Не збережений регламент, недостатньо розкрита тема", (int)Variant.Value.Medium},
            { "Присутні помилки в регламенті і використання плакатів", (int)Variant.Value.Normal},
            { "Дотримання часу, розкриття актуальності, новизни, з використанням плакатів", (int)Variant.Value.VeryHigh},
        };

        Dictionary<string, int> yakist2 = new Dictionary<string, int>() {
            { "Не відповідають доповіді, виповнені на низькому рівні", (int)Variant.Value.Low},
            { "Не повністю відповідають змісту доповіді, присутні помилки в оформленні і відхилення від стандартів", (int)Variant.Value.Medium},
            { "Присутні невеликі похибки в оформленні", (int)Variant.Value.High },
            { "Повнітю відповідає змісту доповіді, доповнюють її, відповідають на вимоги стандартів", (int) Variant.Value.VeryHigh },
        };

        Dictionary<string, int> yakistVidpov = new Dictionary<string, int>() {
            { "Не може відповісти на додаткові питання", (int) Variant.Value.VeryBad },
            { "Незнання основного матеріалу", (int)Variant.Value.VeryLow},
            { "Присутні помилки у відповіді", (int)Variant.Value.NotBad },
            { "Висока ерудиція, відсутні істотні помилки", (int)Variant.Value.VeryHigh },
            { "Точні відповіді, високий рівень ерудиції", (int)Variant.Value.Excellent },
        };

        Dictionary<string, int> dotrStand = new Dictionary<string, int>() {
            { "Робота не дотримується стандартів", 0},
            { "Робота дещо не відповідає станартам ", 1 },
            { "Робота повіністю відповідає стандартам", 2 },
        };

        Dictionary<string, int> zagOcinka = new Dictionary<string, int>() {
            { "2", (int) Variant.Value.Bad },
            { "3", (int) Variant.Value.Medium },
            { "4", (int) Variant.Value.High },
            { "5", (int) Variant.Value.Excellent },
        };

        private List<ComboBox> comboBoxes1 = new List<ComboBox>();
        private List<ComboBox> comboBoxes2 = new List<ComboBox>();
        private List<ComboBox> comboBoxes3 = new List<ComboBox>();

        private void Form1_Load(object sender, EventArgs e)
        {
            //GroupBox1
            this.comboBox1.DataSource = new BindingSource(honesty, null);
            this.comboBox1.DisplayMember = "Key";
            this.comboBox1.ValueMember = "Value";
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes1.Add(comboBox1);

            this.comboBox2.DataSource = new BindingSource(litWork, null);
            this.comboBox2.DisplayMember = "Key";
            this.comboBox2.ValueMember = "Value";
            this.comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes1.Add(comboBox2);

            this.comboBox3.DataSource = new BindingSource(goal, null);
            this.comboBox3.DisplayMember = "Key";
            this.comboBox3.ValueMember = "Value";
            this.comboBox3.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes1.Add(comboBox3);

            this.comboBox4.DataSource = new BindingSource(samost, null);
            this.comboBox4.DisplayMember = "Key";
            this.comboBox4.ValueMember = "Value";
            this.comboBox4.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes1.Add(comboBox4);

            //GroupBox2
            this.comboBox5.DataSource = new BindingSource(actual, null);
            this.comboBox5.DisplayMember = "Key";
            this.comboBox5.ValueMember = "Value";
            this.comboBox5.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox5);

            this.comboBox6.DataSource = new BindingSource(litReview, null);
            this.comboBox6.DisplayMember = "Key";
            this.comboBox6.ValueMember = "Value";
            this.comboBox6.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox6);

            this.comboBox7.DataSource = new BindingSource(newness, null);
            this.comboBox7.DisplayMember = "Key";
            this.comboBox7.ValueMember = "Value";
            this.comboBox7.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox7);

            this.comboBox8.DataSource = new BindingSource(desition, null);
            this.comboBox8.DisplayMember = "Key";
            this.comboBox8.ValueMember = "Value";
            this.comboBox8.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox8);

            this.comboBox9.DataSource = new BindingSource(gram, null);
            this.comboBox9.DisplayMember = "Key";
            this.comboBox9.ValueMember = "Value";
            this.comboBox9.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox9);

            this.comboBox10.DataSource = new BindingSource(pract, null);
            this.comboBox10.DisplayMember = "Key";
            this.comboBox10.ValueMember = "Value";
            this.comboBox10.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox10);

            this.comboBox11.DataSource = new BindingSource(dotrStand, null);
            this.comboBox11.DisplayMember = "Key";
            this.comboBox11.ValueMember = "Value";
            this.comboBox11.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox11);

            this.comboBox12.DataSource = new BindingSource(graphMat, null);
            this.comboBox12.DisplayMember = "Key";
            this.comboBox12.ValueMember = "Value";
            this.comboBox12.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox12);

            this.comboBox13.DataSource = new BindingSource(naukTeh, null);
            this.comboBox13.DisplayMember = "Key";
            this.comboBox13.ValueMember = "Value";
            this.comboBox13.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes2.Add(comboBox13);

            //GroupBox3
            this.comboBox14.DataSource = new BindingSource(yakistDok, null);
            this.comboBox14.DisplayMember = "Key";
            this.comboBox14.ValueMember = "Value";
            this.comboBox14.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes3.Add(comboBox14);

            this.comboBox15.DataSource = new BindingSource(yakist2, null);
            this.comboBox15.DisplayMember = "Key";
            this.comboBox15.ValueMember = "Value";
            this.comboBox15.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes3.Add(comboBox15);

            this.comboBox16.DataSource = new BindingSource(yakistVidpov, null);
            this.comboBox16.DisplayMember = "Key";
            this.comboBox16.ValueMember = "Value";
            this.comboBox16.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes3.Add(comboBox16);

            this.comboBox17.DataSource = new BindingSource(zagOcinka, null);
            this.comboBox17.DisplayMember = "Key";
            this.comboBox17.ValueMember = "Value";
            this.comboBox17.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxes3.Add(comboBox17);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButtonCalculationType2.Checked)
            {
                MessageBox.Show("Result is: " + CalculateResult());
                return;
            }
            double[][] neuronOutputX = new double[4][];
            int index = 0;
            foreach (ComboBox cb in comboBoxes1)
            {
                int selectedValue = ((KeyValuePair<string, int>)cb.SelectedItem).Value;
                int criteriaLength = cb.Items.Count;
                CriteriaNeuron cn = new CriteriaNeuron(selectedValue, criteriaLength);
                neuronOutputX[index] = cn.GetOutput();
                index++;
            }
            double[] X = CalcX(neuronOutputX);
            MessageBox.Show(string.Join(", ", X), "First Neuron", MessageBoxButtons.OK, MessageBoxIcon.Information);

            double[][] neuronOutputY = new double[9][];
            index = 0;
            foreach (ComboBox cb in comboBoxes2)
            {
                int selectedValue = ((KeyValuePair<string, int>)cb.SelectedItem).Value;
                int criteriaLength = cb.Items.Count;
                CriteriaNeuron cn = new CriteriaNeuron(selectedValue, criteriaLength);
                neuronOutputY[index] = cn.GetOutput();
                index++;
            }
            double[] Y = CalcY(neuronOutputY);
            MessageBox.Show(string.Join(", ", Y), "Second Neuron", MessageBoxButtons.OK, MessageBoxIcon.Information);

            double[][] neuronOutputZ = new double[4][];
            index = 0;
            foreach (ComboBox cb in comboBoxes3)
            {
                int selectedValue = ((KeyValuePair<string, int>)cb.SelectedItem).Value;
                int criteriaLength = cb.Items.Count;
                CriteriaNeuron cn = new CriteriaNeuron(selectedValue, criteriaLength);
                neuronOutputZ[index] = cn.GetOutput();
                index++;
            }
            double[] Z = CalcZ(neuronOutputZ);
            MessageBox.Show(string.Join(", ", Z), "Third Neuron", MessageBoxButtons.OK, MessageBoxIcon.Information);

            MessageBox.Show(CalcR(X, Y, Z).ToString(), "Third Neuron", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public double CalcR(double[] X, double[] Y, double[] Z)
        {
            double[] values = new double[7];
            values[0] = X[0] * Y[0] * Z[0];
            values[1] = X[0] * Y[0] * Z[0];
            values[2] = X[0] * Y[1] * Z[1];
            values[3] = X[1] * Y[2] * Z[2];
            values[4] = X[1] * Y[3] * Z[3];
            values[5] = X[2] * Y[3] * Z[3];
            values[6] = X[2] * Y[4] * Z[4];

            double sum1 = 0;
            double sum2 = 0;

            for (int i = 1; i < 8; i++)
            {
                sum1 += (1 + (i - 1)) * ((7 - 1) / (7 - 1)) * values[i - 1];
                sum2 += values[i - 1];
            }

            return sum1 / sum2;
        }

        public double[] CalcX(double[][] X)
        {
            double max = 0;
            double[] values = new double[4];
            values[0] = X[0][0] * X[1][0] * X[2][0] * X[3][0];
            values[1] = X[0][0] * X[1][1] * X[2][1] * X[3][1];
            values[2] = X[0][1] * X[1][2] * X[2][2] * X[3][2];
            //return max = Math.Max(values[2], Math.Max(values[0], values[1]));
            return values;
        }

        public double[] CalcY(double[][] X)
        {
            double max = 0;
            double[] values = new double[5];
            values[0] = X[0][0] * X[1][0] * X[2][0] * X[3][0] * X[4][0] * X[5][0] * X[6][0] * X[7][0] * X[8][0];
            values[1] = X[0][0] * X[1][1] * X[2][0] * X[3][1] * X[4][0] * X[5][0] * X[6][0] * X[7][0] * X[8][1];
            values[2] = X[0][1] * X[1][2] * X[2][1] * X[3][2] * X[4][1] * X[5][1] * X[6][1] * X[7][1] * X[8][2];
            values[3] = X[0][1] * X[1][3] * X[2][1] * X[3][3] * X[4][1] * X[5][1] * X[6][1] * X[7][1] * X[8][3];
            values[4] = X[0][2] * X[1][4] * X[2][2] * X[3][4] * X[4][2] * X[5][2] * X[6][2] * X[7][2] * X[8][4];
            //return max = Math.Max(values[0], Math.Max(values[1], Math.Max(values[2], Math.Max(values[3], values[4]))));
            return values;
        }

        public double[] CalcZ(double[][] X)
        {
            double max = 0;
            double[] values = new double[5];
            values[0] = X[0][0] * X[1][0] * X[2][0] * X[3][0];
            values[1] = X[0][0] * X[1][0] * X[2][1] * X[3][0];
            values[2] = X[0][1] * X[1][1] * X[2][2] * X[3][1];
            values[3] = X[0][2] * X[1][2] * X[2][3] * X[3][2];
            values[4] = X[0][3] * X[1][3] * X[2][4] * X[3][3];
            //return max = Math.Max(values[0], Math.Max(values[1], Math.Max(values[2], Math.Max(values[3], values[4]))));
            return values;
        }

    }


    public class CriteriaNeuron
    {
        public CriteriaNeuron(int value, int len)
        {
            Length = len; 
            Value = value;
        }
        public int Length { get; set; }
        public int Value { get; set; }
        private const int MIN_VALUE = 0;
        private const double COOFICIENT = 0.9;

        public double[] GetOutput()
        {
            double[] output = new double[this.Length];
            double value = (double)(Length - 1 * ((Value - CriteriaNeuron.MIN_VALUE) / ((Length - 1) - CriteriaNeuron.MIN_VALUE)));

            for (int i = 0; i < Length; i++)
            {
                output[i] = (1 / (1 + ((value - i) / CriteriaNeuron.COOFICIENT)));
            }

            return output;
        }
    }
}
