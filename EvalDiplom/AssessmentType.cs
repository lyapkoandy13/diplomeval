﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EvalDiplom
{
    class AssessmentType
    {
        public enum Value : int
        {
            VeryLow = 2,
            Low = 4,
            Medium = 6,
            High = 8,
            VeryHigh = 10
        }

        public Value value;
        public List<Variant> variants;
        public ComboBox comboBox;

        public AssessmentType(Value value, List<Variant> variants, ComboBox comboBox)
        {
            this.value = value;
            this.variants = variants;
            this.comboBox = comboBox;
        }

        public double Evaluation
        {
            get
            {
                Variant.Value selectedValue = (Variant.Value)((KeyValuePair<string, int>)comboBox.SelectedItem).Value;

                int max = 0;
                int min = int.MaxValue;
                foreach (Variant variant in variants)
                {
                    max = Math.Max((int)variant.value, max);
                    min = Math.Min((int)variant.value, min);
                }

                return ((double)selectedValue - min) / (max - min);
            }
        }

        public static AssessmentType fromDictionary(Value value, Dictionary<string, int> dict, ComboBox comboBox)
        {
            List<Variant> variants = new List<Variant>();

            foreach (KeyValuePair<string, int> variant in dict)
                variants.Add(Variant.fromPair(variant));

            return new AssessmentType(value, variants, comboBox);
        }
    }
}
