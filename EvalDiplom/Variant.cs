﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvalDiplom
{
    class Variant
    {
        public enum Value : int
        {
            VeryBad = 1,
            Bad = 2,
            VeryLow = 3,
            Low = 4,
            Medium = 5,
            NotBad = 6,
            Normal = 7,
            High = 8,
            VeryHigh = 9,
            Excellent = 10
        }

        public Value value;

        public Variant(Value value)
        {
            this.value = value;
        }

        public static Variant fromPair(KeyValuePair<string, int> value)
        {
            return new Variant((Value)value.Value);
        }
    }
}